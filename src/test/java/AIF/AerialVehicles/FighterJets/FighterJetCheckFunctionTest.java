package AIF.AerialVehicles.FighterJets;

import AIF.AIFUtil;
import AIF.AerialVehicles.AerialVehicle;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class FighterJetCheckFunctionTest {
    private final int value;
    private final int expected;
    private static final String PLANE_NAME = "F15";
    public static AIFUtil aifUtil;

    @Parameterized.Parameters(name = "{index}: given input {0}, answer should be {1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {50, 50}, {3, 3}, {200, 200}, // in group
                {0, 0}, {-1, 0}, {1, 1}, {249, 249}, {250, 0}, {251, 0}, // boundaries
                {-6, 0}, {742, 0} // out of group
        });
    }

    @BeforeClass
    public static void beforeAll() {
        aifUtil = new AIFUtil();
    }

    public FighterJetCheckFunctionTest(int value, int expected) {
        this.value = value;
        this.expected = expected;
    }

    @Test
    public void testCheckFighterJet() {
        aifUtil.setHoursAndCheck(getFighterJet(), value);
        assertEquals("failure - hoursOfFlightSinceLastRepair should be " + expected, expected, getFighterJet().getHoursOfFlightSinceLastRepair());
    }

    private AerialVehicle getFighterJet() {
        return aifUtil.getAerialVehiclesHashMap().get(PLANE_NAME);
    }
}
