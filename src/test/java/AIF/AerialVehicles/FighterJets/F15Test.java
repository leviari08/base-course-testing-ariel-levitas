package AIF.AerialVehicles.FighterJets;

import AIF.AIFUtil;
import AIF.AerialVehicles.AerialVehicle;
import AIF.Missions.MissionTypeException;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class F15Test {
    public static AIFUtil aifUtil;
    private static AerialVehicle F15;

    @BeforeClass
    public static void beforeAll() {
        aifUtil = new AIFUtil();
        F15 = aifUtil.getAerialVehiclesHashMap().get("F15");
    }

    @Test
    public void testF15SetMissions() {
        try {
            F15.setMission(aifUtil.getAllMissions().get("attack"));
            assertTrue(true);
        } catch (MissionTypeException error) {
            fail();
        }
    }

    @Test(expected = MissionTypeException.class)
    public void testF15SetBDAMission() throws MissionTypeException {
        F15.setMission(aifUtil.getAllMissions().get("bda"));
    }

    @Test
    public void testF15SetIntelligenceMission() {
        try {
            F15.setMission(aifUtil.getAllMissions().get("intelligence"));
            assertTrue(true);
        } catch (MissionTypeException error) {
            fail();
        }
    }
}