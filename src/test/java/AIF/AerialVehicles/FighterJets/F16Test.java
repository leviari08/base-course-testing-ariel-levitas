package AIF.AerialVehicles.FighterJets;

import AIF.AIFUtil;
import AIF.AerialVehicles.AerialVehicle;
import AIF.Missions.MissionTypeException;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class F16Test {
    public static AIFUtil aifUtil;
    private static AerialVehicle F16;

    @BeforeClass
    public static void beforeAll() {
        aifUtil = new AIFUtil();
        F16 = aifUtil.getAerialVehiclesHashMap().get("F16");
    }

    @Test
    public void testF16SetMissions() {
        try {
            F16.setMission(aifUtil.getAllMissions().get("attack"));
            assertTrue(true);
        } catch (MissionTypeException error) {
            fail();
        }
    }

    @Test
    public void testF16SetBDAMission() {
        try {
            F16.setMission(aifUtil.getAllMissions().get("bda"));
            assertTrue(true);
        } catch (MissionTypeException error) {
            fail();
        }
    }

    @Test(expected = MissionTypeException.class)
    public void testF16SetIntelligenceMission() throws MissionTypeException {
        F16.setMission(aifUtil.getAllMissions().get("intelligence"));
    }
}