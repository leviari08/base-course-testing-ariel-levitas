package AIF.AerialVehicles.UAVs.Hermes;

import AIF.AIFUtil;
import AIF.AerialVehicles.AerialVehicle;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class HermesCheckFunctionTest {
    private final int value;
    private final int expected;
    private static final String PLANE_NAME = "Kohav";
    public static AIFUtil aifUtil;

    @Parameterized.Parameters(name = "{index}: given input {0}, answer should be {1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {50, 50}, {3, 3}, {80, 80}, // in group
                {0, 0}, {-1, 0}, {1, 1}, {99, 99}, {100, 0}, {101, 0}, // boundaries
                {-6, 0}, {742, 0} // out of group
        });
    }

    @BeforeClass
    public static void beforeAll() {
        aifUtil = new AIFUtil();
    }

    public HermesCheckFunctionTest(int value, int expected) {
        this.value = value;
        this.expected = expected;
    }

    @Test
    public void testCheckHermes() {
        aifUtil.setHoursAndCheck(getHermes(), value);
        assertEquals("failure - hoursOfFlightSinceLastRepair should be " + expected, expected, getHermes().getHoursOfFlightSinceLastRepair());
    }

    private AerialVehicle getHermes() {
        return aifUtil.getAerialVehiclesHashMap().get(PLANE_NAME);
    }
}
