package AIF.AerialVehicles.UAVs.Hermes;

import AIF.AIFUtil;
import AIF.AerialVehicles.AerialVehicle;
import AIF.Missions.AttackMission;
import AIF.Missions.MissionTypeException;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class ZikTest {
    public static AIFUtil aifUtil;
    private static AerialVehicle zik;

    @BeforeClass
    public static void beforeAll() {
        aifUtil = new AIFUtil();
        zik = aifUtil.getAerialVehiclesHashMap().get("Zik");
    }

    @Test(expected = MissionTypeException.class)
    public void testZikSetMissions() throws MissionTypeException {
        zik.setMission(aifUtil.getAllMissions().get("attack"));
    }

    @Test
    public void testZikSetBDAMission() {
        try {
            zik.setMission(aifUtil.getAllMissions().get("bda"));
            assertTrue(true);
        } catch (MissionTypeException error) {
            fail();
        }
    }

    @Test
    public void testZikSetIntelligenceMission() {
        try {
            zik.setMission(aifUtil.getAllMissions().get("intelligence"));
            assertTrue(true);
        } catch (MissionTypeException error) {
            fail();
        }
    }
}