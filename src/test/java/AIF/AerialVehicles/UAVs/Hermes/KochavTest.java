package AIF.AerialVehicles.UAVs.Hermes;

import AIF.AIFUtil;
import AIF.AerialVehicles.AerialVehicle;
import AIF.Missions.MissionTypeException;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class KochavTest {
    public static AIFUtil aifUtil;
    private static AerialVehicle kochav;

    @BeforeClass
    public static void beforeAll() {
        aifUtil = new AIFUtil();
        kochav = aifUtil.getAerialVehiclesHashMap().get("Kohav");
    }

    @Test
    public void testKochavSetMissions() {
        try {
            kochav.setMission(aifUtil.getAllMissions().get("attack"));
            assertTrue(true);
        } catch (MissionTypeException error) {
            fail();
        }
    }

    @Test
    public void testKochavSetBDAMission() {
        try {
            kochav.setMission(aifUtil.getAllMissions().get("bda"));
            assertTrue(true);
        } catch (MissionTypeException error) {
            fail();
        }
    }

    @Test
    public void testKochavSetIntelligenceMission() {
        try {
            kochav.setMission(aifUtil.getAllMissions().get("intelligence"));
            assertTrue(true);
        } catch (MissionTypeException error) {
            fail();
        }
    }
}