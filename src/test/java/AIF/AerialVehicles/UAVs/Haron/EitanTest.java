package AIF.AerialVehicles.UAVs.Haron;

import AIF.AIFUtil;
import AIF.AerialVehicles.AerialVehicle;
import AIF.Missions.MissionTypeException;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class EitanTest {
    public static AIFUtil aifUtil;
    private static AerialVehicle Eitan;

    @BeforeClass
    public static void beforeAll() {
        aifUtil = new AIFUtil();
        Eitan = aifUtil.getAerialVehiclesHashMap().get("Eitan");
    }

    @Test
    public void testEitanSetMissions() {
        try {
            Eitan.setMission(aifUtil.getAllMissions().get("attack"));
            assertTrue(true);
        } catch (MissionTypeException error) {
            fail();
        }
    }

    @Test(expected = MissionTypeException.class)
    public void testEitanSetBDAMission() throws MissionTypeException {
        Eitan.setMission(aifUtil.getAllMissions().get("bda"));
    }

    @Test
    public void testEitanSetIntelligenceMission() {
        try {
            Eitan.setMission(aifUtil.getAllMissions().get("intelligence"));
            assertTrue(true);
        } catch (MissionTypeException error) {
            fail();
        }
    }
}