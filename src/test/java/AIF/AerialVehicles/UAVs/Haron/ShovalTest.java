package AIF.AerialVehicles.UAVs.Haron;

import AIF.AIFUtil;
import AIF.AerialVehicles.AerialVehicle;
import AIF.Missions.MissionTypeException;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class ShovalTest {
    public static AIFUtil aifUtil;
    private static AerialVehicle Shoval;

    @BeforeClass
    public static void beforeAll() {
        aifUtil = new AIFUtil();
        Shoval = aifUtil.getAerialVehiclesHashMap().get("Shoval");
    }

    @Test
    public void testShovalSetMissions() {
        try {
            Shoval.setMission(aifUtil.getAllMissions().get("attack"));
            assertTrue(true);
        } catch (MissionTypeException error) {
            fail();
        }
    }

    @Test
    public void testShovalSetBDAMission() {
        try {
            Shoval.setMission(aifUtil.getAllMissions().get("bda"));
            assertTrue(true);
        } catch (MissionTypeException error) {
            fail();
        }
    }

    @Test
    public void testShovalSetIntelligenceMission() {
        try {
            Shoval.setMission(aifUtil.getAllMissions().get("intelligence"));
            assertTrue(true);
        } catch (MissionTypeException error) {
            fail();
        }
    }
}