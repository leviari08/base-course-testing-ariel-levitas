package AIF.AerialVehicles.UAVs.Haron;

import AIF.AIFUtil;
import AIF.AerialVehicles.AerialVehicle;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class HaronCheckFunctionTest {
    private final int value;
    private final int expected;
    private static final String PLANE_NAME = "Shoval";
    public static AIFUtil aifUtil;

    @Parameterized.Parameters(name = "{index}: given input {0}, answer should be {1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {50, 50}, {3, 3}, {140, 140}, // in group
                {0, 0}, {-1, 0}, {1, 1}, {149, 149}, {150, 0}, {151, 0}, // boundaries
                {-6, 0}, {742, 0} // out of group
        });
    }

    @BeforeClass
    public static void beforeAll() {
        aifUtil = new AIFUtil();
    }

    public HaronCheckFunctionTest(int value, int expected) {
        this.value = value;
        this.expected = expected;
    }

    @Test
    public void testCheckHaron() {
        aifUtil.setHoursAndCheck(getHaron(), value);
        assertEquals("failure - hoursOfFlightSinceLastRepair should be " + expected, expected, getHaron().getHoursOfFlightSinceLastRepair());
    }

    private AerialVehicle getHaron() {
        return aifUtil.getAerialVehiclesHashMap().get(PLANE_NAME);
    }
}
