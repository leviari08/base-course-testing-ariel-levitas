package AIF.AerialVehicles;

import AIF.AIFUtil;
import AIF.AerialVehicles.UAVs.Hermes.Zik;
import AIF.Missions.BdaMission;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class AerialVehicleTest {
    private final int value;
    private final int expected;
    public static AIFUtil aifUtil;
    private static BdaMission mockBDAMission;

    @Parameterized.Parameters(name = "{index}: given input {0}, answer should be {1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {-17, 0}, {0, 0}, {5, 5}
        });
    }

    @BeforeClass
    public static void beforeAll() {
        aifUtil = new AIFUtil();
        mockBDAMission = (BdaMission) aifUtil.getAllMissions().get("bda");
    }

    public AerialVehicleTest(int value, int expected) {
        this.value = value;
        this.expected = expected;
    }

    @Test
    public void AerialVehicleConstructorTest() {
        Zik zik = new Zik("thermal", "elint", "Mr.Pilot", mockBDAMission, value, true);
        assertEquals("hoursSinceLastFlight should not be less than zero " + expected, expected, zik.getHoursOfFlightSinceLastRepair());
    }

    @Test
    public void AerialVehicleSetHoursOfFlightSinceLastRepairTest() {
        AerialVehicle zik = aifUtil.getAerialVehiclesHashMap().get("Zik");
        zik.setHoursOfFlightSinceLastRepair(value);
        assertEquals("failure - hoursOfFlightSinceLastRepair should be " + expected, expected, zik.getHoursOfFlightSinceLastRepair());
    }
}